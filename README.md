# Scratch That

Scratch That is a minimalist note-taking tool written in PHP. Scratch That stores content is in a plain text file. The editor create a backup copy of the text file on each save. You can protect access to the editor with a password.

## Dependencies

- PHP 7.x or higher
- Apache or any other web server (optional)

# Installation and Usage

To run Scratch That locally, run the `php -S localhost:8000` command and point your browser to *localhost:8000*

To deploy Scratch That on a web server, move the *scratchthat* directory to the document root of the server. Point your browser to *127.0.0.1/scratchthat* (replace *127.0.0.1* with the actual IP adress or domain name of your server).
