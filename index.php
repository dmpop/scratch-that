<?php
include('config.php');
if ($protect) {
	require_once('protect.php');
}
error_reporting(E_ERROR);
?>
<html lang="en" data-theme="<?php echo $theme ?>">
<!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->

<head>
	<title>Scratch That</title>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/classless.css" />
	<style>
		textarea {
			font-size: 15px;
			width: 100%;
			height: 70%;
			line-height: 1.9;
			margin-top: 2em;
		}
	</style>
</head>

<body>
	<div class="text-center">
		<h1 style="margin-top: 0em;">Scratch That</h1>
		<?php
		if (!file_exists($dir)) {
			mkdir($dir, 0777, true);
		}
		$mdfile = $dir . "/content.md";
		function Read()
		{
			include('config.php');
			$mdfile = $dir . "/content.md";
			echo file_get_contents($mdfile);
		}
		function Write()
		{
			include('config.php');
			$mdfile = $dir . "/content.md";
			copy($mdfile, $dir . '/' . date('Y-m-d-H-i-s') . '.md');
			$fp = fopen($mdfile, "w");
			$data = $_POST["text"];
			fwrite($fp, $data);
			fclose($fp);
		}
		?>
		<?php
		if (isset($_POST["save"])) {
			Write();
		};
		?>
		<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
			<textarea name="text"><?php Read(); ?></textarea>
			<button style="margin-top: 2em;" type="submit" name="save">Save</button>
		</form>
	</div>
</body>

</html>