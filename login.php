<?php
$config = include('config.php');

/* Redirects here after login */
$redirect_after_login = 'index.php';

/* Set timezone to UTC */

date_default_timezone_set('UTC');

/* Will not ask password again for */
$remember_password = strtotime('+30 days'); // 30 days

if (isset($_POST['password']) && $_POST['password'] == $passwd) {
    setcookie("password", $passwd, $remember_password);
    header('Location: ' . $redirect_after_login);
    exit;
}
?>
<!DOCTYPE html>
<html lang="en" data-theme="<?php echo $theme ?>">

<head>
    <title>Scratch That</title>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/classless.css" />
</head>

<body>

    <div class="text-center">
        <h1 style="margin-top: 0em;">Scratch That</h1>
        <div class="card">
            <form method="POST">
                <p>Type password and press ENTER:</p>
                <input type="password" name="password">
            </form>
        </div>
    </div>
</body>

</html>